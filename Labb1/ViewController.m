//
//  ViewController.m
//  Labb1
//
//  Created by Viktor Jegerås on 2015-01-20.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIView *colorBlock;

@property (weak, nonatomic) IBOutlet UIButton *colorButton;

@end

@implementation ViewController

- (UIColor*) currentColor {
    return [UIColor colorWithRed:drand48()
                           green:drand48()
                            blue:drand48()
                           alpha:1.0f];
}
- (IBAction)changeColor:(id)sender {
    self.colorBlock.backgroundColor = [self currentColor];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
